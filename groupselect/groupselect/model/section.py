"""Defines the Section model."""

import enum

from sqlalchemy import Column, Enum
from sqlalchemy.orm import relation
from sqlalchemy.types import Integer, Unicode

from groupselect.model import DBSession, DeclarativeBase, StudentTypeEnum


class StateEnum(str, enum.Enum):
    inactive = 'inactive'
    active = 'active'
    selecting = 'selecting'
    finalized = 'finalized'


class Section(DeclarativeBase):
    """A model that defines a class section."""
    __tablename__ = 'sections'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode)
    state = Column(Enum(StateEnum), nullable=False, default=StateEnum.inactive)

    # TODO make configurable?
    num_to_hire = 4
    num_to_accept = 2

    students = relation('Student')

    def advance(self):
        if self.state == StateEnum.active:
            # Calculate how many managers we need.
            managers_needed = len(self.students) // 3
            candidates_needed = managers_needed * 2

            # Ensure that we have enough people for the process (need at least
            # n/3 people).
            num_signed_in = sum(
                map(lambda s: 1 if s.signed_in else 0, self.students))
            if num_signed_in < managers_needed:
                raise AssertionError(
                    'Not enough people are signed in to start the group selection process.'
                )

            # Sort the students by their manager rank.
            students = sorted(self.students, key=lambda s: s.manager_order)
            for s in students:
                if not s.signed_in:
                    s.type = StudentTypeEnum.exempt
                    continue

                if managers_needed > 0:
                    s.type = StudentTypeEnum.manager
                    managers_needed -= 1
                elif candidates_needed > 0:
                    s.type = StudentTypeEnum.candidate
                    candidates_needed -= 1
                else:
                    s.type = StudentTypeEnum.exempt

        # Advance the state
        state_order = [
            StateEnum.inactive,
            StateEnum.active,
            StateEnum.selecting,
            StateEnum.finalized,
        ]
        current_state = state_order.index(self.state)
        if current_state < 3:
            self.state = state_order[current_state + 1]

        DBSession.flush()
