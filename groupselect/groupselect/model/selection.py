"""Defines the Selection model."""

import datetime

from sqlalchemy import Column, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relation
from sqlalchemy.types import DateTime, Integer, Text, Unicode

from groupselect.model import DeclarativeBase
from tg import config


class Selection(DeclarativeBase):
    """A model that defines a manager's selection."""
    __tablename__ = 'selections'

    id = Column(Integer, primary_key=True)
    manager_id = Column(Integer, ForeignKey('student.id'))
    person_id = Column(Integer, ForeignKey('student.id'))

    __table_args__ = (UniqueConstraint(
        'manager_id',
        'person_id',
        name='_selection_manager_person_unique',
    ), )

    manager = relation(
        'Student', primaryjoin='Student.id == Selection.manager_id')
    candidate = relation(
        'Student', primaryjoin='Student.id == Selection.person_id')
