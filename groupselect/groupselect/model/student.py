"""Defines the Student model."""

import enum

from sqlalchemy import Column, Enum, ForeignKey
from sqlalchemy.types import Boolean, Integer, Unicode, Float

from groupselect.model import DBSession, DeclarativeBase, Selection


class StudentTypeEnum(str, enum.Enum):
    unassigned = 'unassigned'
    manager = 'manager'
    candidate = 'candidate'
    exempt = 'exempt'


class Student(DeclarativeBase):
    """A model that defines a student's basic info"""
    __tablename__ = 'student'

    id = Column(Integer, primary_key=True)
    cwid = Column(Integer, nullable=False)
    name = Column(Unicode, nullable=False)
    username = Column(Unicode, nullable=False)
    type = Column(
        Enum(StudentTypeEnum),
        nullable=False,
        default=StudentTypeEnum.unassigned)
    signed_in = Column(Boolean, default=False, nullable=False)
    section = Column(
        Integer,
        ForeignKey('sections.id'),
        nullable=False,
    )

    manager_order = Column(Integer)
    grade_rank = Column(Float)

    def compute_manager_state(self):
        num_accepted = 0
        num_hired = 0

        if self.type == StudentTypeEnum.manager:
            num_hired = len(
                DBSession.query(Selection).filter_by(manager_id=self.id).all())
        elif self.type == StudentTypeEnum.candidate:
            num_accepted = len(
                DBSession.query(Selection).filter_by(person_id=self.id).all())

        return dict(
            id=self.id,
            cwid=self.cwid,
            name=self.name,
            type=self.type,
            signed_in=self.signed_in,
            section=self.section,
            accepted=num_accepted,
            hired=num_hired,
        )
