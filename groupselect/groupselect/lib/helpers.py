# -*- coding: utf-8 -*-
"""Template Helpers used in groupselect."""
import logging
from markupsafe import Markup
from datetime import datetime

log = logging.getLogger(__name__)


def app_name():
    return 'Group Selection'


def current_year():
    now = datetime.now()
    return now.strftime('%Y')


# Import commonly used helpers from WebHelpers2 and TG
from tg.util.html import script_json_encode

try:
    from webhelpers2 import date, html, number, misc, text
except SyntaxError:
    log.error("WebHelpers2 helpers not available with this Python Version")
