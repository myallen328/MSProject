# -*- coding: utf-8 -*-
"""Student Controller"""

from groupselect.lib.base import BaseController
from groupselect.model import (DBSession, Section, Selection, StateEnum,
                               Student, StudentTypeEnum)
from tg import expose, flash, redirect, request, response
from tg.i18n import ugettext as _

__all__ = ['StudentController']


class StudentController(BaseController):
    """
    The student controller for the groupselect application.
    """
    def get_student_or_authenticate(self):
        cwid = request.cookies.get('cwid')
        valid_section_states = (
            StateEnum.active,
            StateEnum.selecting,
            StateEnum.finalized,
        )

        if cwid is None:
            redirect('/student/signin')

        try:
            return DBSession.query(Student).join(Section).filter(
                Student.cwid == cwid,
                Section.state.in_(valid_section_states),
            ).order_by(Section.id.desc()).first()
        except:
            response.set_cookie('cwid', '')
            redirect('/student/signin')

    @expose('groupselect.templates.student_signin')
    def index(self):
        """Handle the 'student_signin' page."""
        # If they've signed in, go to selection page.
        if request.cookies.get('cwid'):
            redirect('/student/select')
        return dict(page='student')

    @expose('groupselect.templates.student')
    def select(self):
        student = self.get_student_or_authenticate()
        return dict(page='student', student=student)

    @expose('groupselect.templates.student_signin')
    def signin(self):
        if request.method != 'POST':
            # Not a post, just show the login page.
            return dict(page='student')

        # Find the student.
        cwid = request.params['cwid']
        valid_section_states = (
            StateEnum.active,
            StateEnum.selecting,
            StateEnum.finalized,
        )

        try:
            student = DBSession.query(Student).join(Section).filter(
                Student.cwid == cwid,
                Section.state.in_(valid_section_states),
            ).order_by(Section.id.desc()).one()
        except Exception as e:
            flash(_('Failed to sign in. ' + str(e)), 'error')
            redirect('/student/signin')

        # Sign them in and save to the database.
        student.signed_in = True
        DBSession.flush()

        # Set the cookie and redirect to /select
        response.set_cookie('cwid', cwid)
        redirect('/student/select')

    @expose('json')
    def hire(self):
        manager = self.get_student_or_authenticate()
        # TODO make sure that this doesn't blow up when manager_cwid is not there
        candidate_id = request.params.get('candidate_id')

        if not candidate_id:
            return dict(error='Candidate ID must be specified')

        valid_section_states = (StateEnum.selecting, )

        try:
            # Do not need to sort and use .first here because there should only
            # be one active section with the student.
            candidate = DBSession.query(Student).join(Section).filter(
                Student.id == candidate_id,
                Section.state.in_(valid_section_states),
            ).one()
        except:
            return dict(error='Invalid Candidate ID')

        # Make sure the manager is in fact a manager.
        if manager.type != StudentTypeEnum.manager:
            return dict(error='Only managers can hire')

        # Make sure the candidate is in fact a candidate.
        if candidate.type != StudentTypeEnum.candidate:
            return dict(error='Sorry, only candidates can be hired.')

        # Check counts and make sure the selection is valid
        num_hires = manager.compute_manager_state()["hired"]
        if num_hires >= 4:
            return dict(error='Sorry, ' + manager.name +
                        ' has already hired four candidates.')

        num_accepted = candidate.compute_manager_state()["accepted"]
        if num_accepted >= 2:
            return dict(error='Sorry, ' + candidate.name +
                        ' has already been hired twice.')

        selection = Selection(
            manager_id=manager.id,
            person_id=candidate_id,
        )

        try:
            # Check to make sure that the selection isn't a duplicate
            DBSession.add(selection)
            DBSession.flush()
        except:
            return dict(error='There was an error performing your selection.')

    @expose('json')
    def student_state(self):
        student = self.get_student_or_authenticate()
        return dict(manager_status=student.compute_manager_state())

    @expose('json')
    def selections(self):
        student = self.get_student_or_authenticate()
        names = []

        selections = DBSession.query(Selection)
        if student.type == StudentTypeEnum.manager:
            selections = selections.filter(Selection.manager_id == student.id)
            person_of_interest = lambda s: s.candidate.name
        elif student.type == StudentTypeEnum.candidate:
            selections = selections.filter(Selection.person_id == student.id)
            person_of_interest = lambda s: s.manager.name
        else:
            return dict(names=[])

        return dict(names=list(map(person_of_interest, selections.all())))

    @expose('json')
    def candidate_name(self, id=None):
        # Find the section
        id = int(id)
        valid_section_states = (StateEnum.selecting, )
        try:
            # Using .one here is fine because this should only happen when
            # selections are being made, and therefore, there should only ever
            # be one row returned by this query.
            student = DBSession.query(Student).join(Section).filter(
                Student.id == id,
                Section.state.in_(valid_section_states),
            ).one()

            return dict(name=student.name)
        except:
            # If we can't find the name, return the id used so something useful
            # is displayed.
            return dict(name=id)

    @expose('json')
    def is_finished(self):
        # Find the section
        unfinalized_count = DBSession.query(Student).join(Section).filter(
            Student.cwid == request.cookies.get('cwid'),
            Section.state != StateEnum.finalized,
        ).count()

        return dict(finished=(unfinalized_count == 0))
