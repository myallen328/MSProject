function AdminFinalize() {
  //
  // Store all of the element on the page as variables.
  //

  // Top panel
  const section_selector = $('#section-selector');
  const export_selections = $('#export-selections');
  const form_groups = $('#form-groups');
  const groups_json = $('#groups-json');

  // Tables
  const exempt_badge = $('#exempt-badge');
  const exempt_table = $('#exempt-body');
  const remaining_candidates_table = $('#remaining-candidates-body');
  const remaining_candidates_badge = $('#remaining-candidates-badge');
  const current_groups_body = $('#current-groups-body');
  const current_groups_badge = $('#current-groups-badge');

  //
  // Global State
  //
  const state = {
    loading: true,

    // Section State
    current_groups: [],
    exempt: [],
    remaining_candidates: [],

    // Potentially Configurable Options
    num_to_hire: 4,
    num_to_accept: 2,
  };

  //
  // Server Communication Code
  //
  const get_relative_url = url => {
    let root_url = window.root_url;
    // Remove the trailing "/"
    root_url = root_url.substring(0, root_url.length - 1);
    return root_url + url;
  };

  const get_section_state = (section_id, callback) => {
    $.getJSON(get_relative_url(`/admin/section_state?section=${section_id}&for_finalize=True`),
      (data, status) => {
        if (status !== 'success') {
        // TODO handle this nicely
          console.log('Failed to retrieve section state.');
        }
        callback(data);
      }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to retrieve section state.');
    });
  };

  const delete_selection = (manager_id, candidate_id, callback) => {
    $.getJSON(get_relative_url(`/admin/delete_selection?manager_id=${manager_id}&candidate_id=${candidate_id}`),
      (data, status) => {
        if (status !== 'success') {
          // TODO handle this nicely
          console.log('Failed to delete selection.');
        }
        get_section_state(state.selected_section_id, section_state => {
          this.update_state(section_state);
          callback();
        });
      }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to delete selection.');
    });
  };

  const make_selection = (manager_id, candidate_id, callback) => {
    $.getJSON(get_relative_url(`/admin/make_selection?manager_id=${manager_id}&candidate_id=${candidate_id}`),
      (data, status) => {
        if (status !== 'success') {
          // TODO handle this nicely
          console.log('Failed to make selection.');
        }
        get_section_state(state.selected_section_id, section_state => {
          this.update_state(section_state);
          callback();
        });
      }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to make selection.');
    });
  };

  const move_exempt = (candidate_id, callback) => {
    $.getJSON(get_relative_url(`/admin/move_exempt?candidate_id=${candidate_id}`),
      (data, status) => {
        if (status !== 'success') {
          // TODO handle this nicely
          console.log('Failed to make student exempt.');
        }
        get_section_state(state.selected_section_id, section_state => {
          this.update_state(section_state);
          callback();
        });
      }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to make student exempt.');
    });
  };

  const move_to_candidate = (candidate_id, callback) => {
    $.getJSON(get_relative_url(`/admin/move_to_candidate?candidate_id=${candidate_id}`),
      (data, status) => {
        if (status !== 'success') {
          // TODO handle this nicely
          console.log('Failed to make student a candidate.');
        }
        get_section_state(state.selected_section_id, section_state => {
          this.update_state(section_state);
          callback();
        });
      }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to make student a candidate.');
    });
  };

  //
  // Event Listeners
  //
  const section_selector_onchange = () => {
    this.update_state({
      selected_section_id: section_selector.val(), loading: true,
    });
    get_section_state(state.selected_section_id, section_state => {
      this.update_state({ loading: false }, section_state);
    });
  };

  const disabled = (element, disable) => {
    element.prop('disabled', disable);
    if (disable) {
      element.addClass('disabled');
    } else {
      element.removeClass('disabled');
    }
  };

  const bind_listeners = () => {
    $('.delete-from-group').unbind('click');
    $('.move-exempt').unbind('click');
    $('.move-to-candidate').unbind('click');
    $('.remaining-candidate').unbind('dragstart');
    $('.current-group').unbind('dragover');
    $('.current-group').unbind('drop');

    section_selector.change(section_selector_onchange);
    $('.delete-from-group').on('click', e => {
      this.update_state({ loading: true });
      delete_selection(e.currentTarget.dataset.managerid, e.currentTarget.dataset.candidateid,
        () => this.update_state({ loading: false }));
    });

    $('.move-exempt').on('click', e => {
      this.update_state({ loading: true });
      move_exempt(e.currentTarget.dataset.candidateid,
        () => this.update_state({ loading: false }));
    });

    $('.move-to-candidate').on('click', e => {
      this.update_state({ loading: true });
      move_to_candidate(e.currentTarget.dataset.candidateid,
        () => this.update_state({ loading: false }));
    });

    $('.remaining-candidate').on('dragstart', e => {
      e.originalEvent.dataTransfer.setData('text/plain', e.target.dataset.candidateid);
    });

    $('.current-group').on('dragover', e => {
      e.preventDefault();
      e.originalEvent.dataTransfer.dropEffect = 'move';
    });

    $('.current-group').on('drop', e => {
      e.preventDefault();
      const candidate_id = e.originalEvent.dataTransfer.getData('text/plain');
      this.update_state({ loading: true });
      make_selection(e.currentTarget.dataset.managerid, candidate_id,
        () => this.update_state({ loading: false }));
    });
  };

  //
  // Helper Functions
  //
  const count = (iterable, fn) => {
    let c = 0;
    for (const el of iterable) {
      if (fn(el)) {
        c++;
      }
    }
    return c;
  };

  //
  // Public Functions
  //
  this.start = () => {
    bind_listeners();
    section_selector_onchange(); // TODO ugly
  };

  this.update_state = (...new_states) => {
    $.extend(state, ...new_states);
    console.log(state);

    this.update_view();
  };

  this.update_view = () => {
    // Disable While Loading or not complete
    const incomplete_groups = state.current_groups.filter(g => g.hirees.length < state.num_to_hire).length;
    disabled(export_selections, state.loading || state.remaining_candidates.length > 0 || incomplete_groups > 0);
    disabled(form_groups, state.loading || state.remaining_candidates.length > 0 || incomplete_groups > 0);
    disabled(groups_json, state.loading || state.remaining_candidates.length > 0 || incomplete_groups > 0);

    // Update the URL for the buttons.
    export_selections[0].href = get_relative_url(`/admin/export_selections?section=${state.selected_section_id}`);
    form_groups[0].href = get_relative_url(`/admin/form_groups?section=${state.selected_section_id}`);
    groups_json[0].href = get_relative_url(`/admin/form_groups?json=true&section=${state.selected_section_id}`);

    // Badges
    exempt_badge.html(state.exempt.length);
    remaining_candidates_badge.html(state.remaining_candidates.length);
    current_groups_badge.html(`${state.current_groups.length - incomplete_groups}/${state.current_groups.length}`);

    // Exempt List
    exempt_table.empty();
    for (const student of state.exempt) {
      exempt_table.append(`
        <tr>
          <td>${student.name}</td>
          <td>
            <button class="move-to-candidate btn btn-default btn-xs"
                    data-candidateid="${student.id}"
                    ${state.loading ? 'disabled' : ''}>
              <span class="glyphicon glyphicon-arrow-down"></span>
            </button>
          </td>
        </tr>
      `);
    }

    // Remaining Candidates List
    remaining_candidates_table.empty();
    for (const c of state.remaining_candidates) {
      const move_arrow = c.accepted === 0 ? `
        <button class="move-exempt btn btn-default btn-xs"
                data-candidateid="${c.id}"
                ${state.loading ? 'disabled' : ''}>
          <span class="glyphicon glyphicon-arrow-up"></span>
        </button>` : '<i>Not allowed</i>';

      remaining_candidates_table.append(`
        <tr class="remaining-candidate" draggable="true" data-candidateid="${c.id}">
          <td>${c.name}</td>
          <td>${c.accepted}</td>
          <td>${move_arrow}</td>
        </tr>
      `);
    }

    // Current Groups List
    current_groups_body.empty();
    for (const group of state.current_groups) {
      const hirees_text = [];
      for (const h of group.hirees) {
        hirees_text.push(`
          ${h.name}
          <a href="javascript:void(0)"
             data-managerid="${group.manager_id}"
             data-candidateid="${h.id}"
             class="delete-from-group glyphicon glyphicon-trash"></a>`);
      }

      current_groups_body.append(`
        <div class="list-group-item current-group"
             data-managerid="${group.manager_id}">
          <div class="pull-left">
            <h4 class="list-group-item-heading">Manager: ${group.manager}</h4>
            <p class="list-group-item-text">Hirees: ${hirees_text.join(', ')}</p>
          </div>
          <div class="pull-right group-check-box
            ${group.hirees.length < state.num_to_hire ? 'incomplete' : 'complete'}">
            <span class="glyphicon glyphicon-ok"></span>
          </div>
          <div class="clearfix"></div>
        </div>
      `);
    }

    bind_listeners();
  };
}

window.app = new AdminFinalize();
window.app.start();
