function App() {
  //
  // Store all of the element on the page as variables.
  //

  // Top panel
  const section_selector = $('#section-selector');
  const section_action = $('#section-action');

  // Progressbar
  const progress_row = $('#progress-row');
  const progress_complete = $('#progress-complete');
  const progress_managers = $('#progress-managers');
  const progress_candidates = $('#progress-candidates');

  // Tables
  const managers_table = $('#signed-in-managers-body');
  const managers_badge = $('#managers-badge');
  const candidates_table = $('#signed-in-candidates-body');
  const candidates_badge = $('#candidates-badge');
  const roster_table = $('#roster-body');
  const roster_badge = $('#roster-badge');

  //
  // Global State
  //
  const state = {
    loading: true,

    // Section State
    selected_section_id: null,
    section_state: null,
    managers: [],
    candidates: [],
    roster: [],

    // Potentially Configurable Options
    num_to_hire: 4,
    num_to_accept: 2,
  };

  let polling_interval = null;

  //
  // Server Communication Code
  //
  const get_relative_url = url => {
    let root_url = window.root_url;
    // Remove the trailing "/"
    root_url = root_url.substring(0, root_url.length - 1);
    return root_url + url;
  };

  const get_section_state = (section_id, callback) => {
    $.getJSON(get_relative_url(`/admin/section_state?section=${section_id}`),
      (data, status) => {
        if (status !== 'success') {
        // TODO handle this nicely
          console.log('Failed to retrieve section state.');
        }
        callback(data);
      }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to retrieve section state.');
    });
  };

  //
  // Event Listeners
  //
  const section_selector_onchange = () => {
    this.update_state({
      selected_section_id: section_selector.val(), loading: true,
    });
    get_section_state(state.selected_section_id, section_state => {
      this.update_state({ loading: false }, section_state);
    });
  };

  const section_action_onclick = () => {
    this.update_state({
      loading: true,
    });
    $.getJSON(get_relative_url(`/admin/advance_state?section=${state.selected_section_id}`),
      (data, status) => {
        if (status !== 'success') {
          // TODO handle this nicely
          console.log('Failed to advance_state.');
        }
        this.update_state({ loading: false }, data);
      })
      .fail(e => {
        // TODO handle this nicely
        alert(e.responseJSON.message.replace(/<br \/>/g, '\n'));
        this.update_state({ loading: false });
      });
  };

  const disabled = (element, disable) => {
    element.prop('disabled', disable);
    if (disable) {
      element.addClass('disabled');
    } else {
      element.removeClass('disabled');
    }
  };

  const bind_listeners = () => {
    section_selector.change(section_selector_onchange);
    section_action.click(section_action_onclick);
  };

  //
  // Helper Functions
  //
  const count = (iterable, fn) => {
    let c = 0;
    for (const el of iterable) {
      if (fn(el)) {
        c++;
      }
    }
    return c;
  };

  const calc_progress = () => {
    const [num_managers, num_candidates, num_roster] = [
      state.managers.length, state.candidates.length, state.roster.length,
    ];
    const total_people = num_managers + num_candidates + num_roster;

    const complete_managers = count(state.managers,
      m => m.hired === state.num_to_hire);
    const complete_candidates = count(state.candidates,
      h => h.accepted === state.num_to_accept);
    const num_present = count(state.roster, s => s.signed_in);

    return {
      complete_managers, num_managers,
      complete_candidates, num_candidates,
      num_present, num_roster,
      complete_percent: (complete_managers + complete_candidates) / total_people * 100,
      manager_percent: (num_managers - complete_managers) / total_people * 100,
      candidates_percent: (num_candidates - complete_candidates) / total_people * 100,
    };
  };

  //
  // Public Functions
  //
  this.start = () => {
    bind_listeners();
    section_selector_onchange(); // TODO ugly
  };

  this.update_state = (...new_states) => {
    $.extend(state, ...new_states);

    if (state.section_state === 'active' || state.section_state === 'selecting') {
      if (polling_interval === null) {
        polling_interval = setInterval(() => {
          get_section_state(state.selected_section_id, section_state => {
            this.update_state(section_state);
          });
        }, 1000);
      }
    } else {
      clearInterval(polling_interval);
      polling_interval = null;
    }

    this.update_view();
  };

  this.update_view = () => {
    // Disable While Loading
    disabled(section_action, state.loading || state.section_state === 'finalized');

    // Section Action Button
    let label = null;
    let cls = '';
    switch (state.section_state) {
      case 'inactive':
        label = 'Activate';
        cls = 'btn-success';
        break;
      case 'active':
        label = 'Start Group Selection Process';
        cls = 'btn-success';
        break;
      case 'selecting':
      case 'finalized':
        label = 'Stop Group Selection Process';
        cls = 'btn-danger';
        break;
      default:
        label = '...';
        cls = 'btn-default';
        break;
    }
    section_action
      .html(label)
      .removeClass('btn-success btn-danger')
      .addClass(cls);

    const progress = calc_progress();

    // Progress Bar
    if (state.section_state === 'selecting') {
      progress_row.removeClass('hidden');
    } else {
      progress_row.addClass('hidden');
    }

    progress_complete.css('width', `${progress.complete_percent}%`);
    progress_managers.css('width', `${progress.manager_percent}%`);
    progress_candidates.css('width', `${progress.candidates_percent}%`);

    // Badges
    managers_badge.html(`${progress.complete_managers}/${progress.num_managers}`);
    candidates_badge.html(`${progress.complete_candidates}/${progress.num_candidates}`);
    roster_badge.html(`${progress.num_present}/${progress.num_roster}`);

    // Managers List
    managers_table.empty();
    for (const manager of state.managers) {
      managers_table.append(`
        <tr class="${manager.hired === state.num_to_hire ? 'success' : ''}">
          <td>${manager.name}</td>
          <td>${manager.hired}</td>
        </tr>
      `);
    }

    // Candidates List
    candidates_table.empty();
    for (const c of state.candidates) {
      candidates_table.append(`
        <tr class="${c.accepted === state.num_to_accept ? 'success' : ''}">
          <td>${c.name}</td>
          <td>${c.accepted}</td>
        </tr>
      `);
    }

    // Not Signed In List
    roster_table.empty();
    for (const student of state.roster) {
      roster_table.append(`
        <tr>
          <td class="${student.signed_in ? 'success' : ''}">${student.name}</td>
        </tr>
      `);
    }
  };
}

const app = new App();
app.start();
