function App() {
  //
  // Store all of the element on the page as variables.
  //

  const signout_link = $('#signout-link');

  const roster_table = $('#manager-selection-body');
  const candidate_table = $('#candidate-selection-body');
  const finished_table = $('#finished-selection-body');

  // Wait Screen
  const wait_section = $('#wait-section');

  // Manager Selector
  const manager_section = $('#manager-section');
  const id_field = $('#candidate-id');
  const selector_button = $('#hire-button');

  // Candidate Selector
  const candidate_section = $('#candidate-section');

  // Finished Screen
  const finished_section = $('#finished-section');
  const exempt_section = $('#exempt-section');

  //
  // Global State
  //
  const state = {
    student: null,

    // Selection Status
    selections: [],

    // Potentially Configurable Options
    num_selections: 0,
  };

  let polling_interval = null;

  //
  // Server Communication Code
  //
  const get_relative_url = url => {
    let root_url = window.root_url;
    // Remove the trailing "/"
    root_url = root_url.substring(0, root_url.length - 1);
    return root_url + url;
  };

  const make_selection = (candidate_id, callback) => {
    $.getJSON(get_relative_url(`/student/hire?candidate_id=${candidate_id}`),
      (data, status) => {
        if (status !== 'success') {
          // TODO handle this nicely
          console.log('Unsuccessful selection.');
        }
        // See if a callback is present, and if so respond with data
        if (data.error) {
          alert(data.error);
        }
        callback && callback(data);
      }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to make a selection.');
    });
  };

  const get_student_state = (callback) => {
    $.getJSON(get_relative_url('/student/student_state'), data => {
      callback(data);
    }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to retrieve student\'s state.');
    });
  };

  const get_selections = (callback) => {
    $.getJSON(get_relative_url('/student/selections'), data => {
      callback && callback(data);
    }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to get selections.');
    });
  };

  const get_name = (id, callback) => {
    $.getJSON(get_relative_url(`/student/candidate_name?id=${id}`), data => {
      callback && callback(data);
    }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to get candidate name.');
    });
  };

  const is_finished = (callback) => {
    $.getJSON(get_relative_url('/student/is_finished'), data => {
      callback && callback(data);
    }).fail(() => {
      // TODO handle this nicely
      console.log('Failed to determine if finished.');
    });
  };

  //
  // Event Listeners
  //

  // Add keybindings here
  const bind_listeners = () => {
    selector_button.click(selector_button_onclick);
    // The or true is necessary because otherwise, no keypresses will work.
    id_field.on('keypress', e => e.keyCode === 13 && selector_button_onclick() || true);

    signout_link.click(signout_click);
  };

  // ---------- Managers Template ---------

  // Make a selection if valid
  const selector_button_onclick = () => {
    get_name(id_field[0].value, resp => {
      if (confirm(`Would you like to hire ${resp.name}?`)) {
        make_selection(id_field[0].value, callback => {
          this.update_view();
        });
      }
    });
  };

  // Signout click. Just delete the cookie.
  const signout_click = () => {
    document.cookie = 'cwid=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT; Max-Age=0';
    window.location.reload();
  }

  //
  // Public Functions
  //
  this.start = () => {
    bind_listeners();

    get_student_state(student_state => {
      state.student = student_state;

      // Determine if the section is complete, and if so, show them the final screen before polling
      is_finished(resp => {
        if (resp.finished) {
          this.update_view();
        } else {
          // Begin polling student state to see when selection period begins
          get_selections(selections => {
            state.selections = selections.names;
            this.active_poll({ student: student_state }, student_state);
          });
        }
      });
    });
  };

  this.active_poll = (...new_states) => {
    $.extend(state, ...new_states);
    if (state.student.manager_status.type === 'unassigned') {
      if (polling_interval === null) {
        polling_interval = setInterval(() => {
          get_student_state(student_state => {
            this.active_poll({ student: student_state }, student_state);
          });
        }, 1000);
      }
    } else {
      clearInterval(polling_interval);
      polling_interval = null;
      // Find out if student is manager, if so show manager view
      status = state.student.manager_status.type;
      if (status === 'manager' && state.selections.length < 4) {
        // Change the template
        wait_section[0].hidden = true;
        manager_section[0].hidden = false;
        candidate_section[0].hidden = true;

        this.selection_poll();
      } else if (status === 'candidate' && state.selections.length < 2) {
        wait_section[0].hidden = true;
        manager_section[0].hidden = true;
        candidate_section[0].hidden = false;

        this.selection_poll();
      } else {
        //   Exempt student
        wait_section[0].hidden = true;
        manager_section[0].hidden = true;
        candidate_section[0].hidden = true;
        exempt_section[0].hidden = false;
    }
      this.update_view();
    }
  };

  this.selection_poll = () => {
    if (finished_section[0].hidden === true) {
      if (polling_interval === null) {
        polling_interval = setInterval(() => {
          this.update_view();
          this.selection_poll();
        }, 1000);
      }
    } else {
      clearInterval(polling_interval);
      polling_interval = null;
    }
  };

  this.update_view = () => {
    // Get the new selections, determine if the student is done
    get_selections(selections => {
      state.selections = selections.names;

      is_finished(res => {
        if ((state.student.manager_status.type === 'manager' && state.selections.length >= 4) ||
                (state.student.manager_status.type === 'candidate' && state.selections.length >= 2) ||
                (state.student.manager_status.type === 'exempt') ||
                res.finished) {
          wait_section[0].hidden = true;
          manager_section[0].hidden = true;
          candidate_section[0].hidden = true;
          finished_section[0].hidden = false;

          // Update table
          finished_table.empty();
          for (const name of state.selections) {
            finished_table.append(`
              <tr>
                <td class="success">${name}</td>
              </tr>
            `);
          }
        } else {
          roster_table.empty();
          for (const name of state.selections) {
            roster_table.append(`
              <tr>
                <td class="success">${name}</td>
              </tr>
            `);
          }

          candidate_table.empty();
          for (const name of state.selections) {
            candidate_table.append(`
              <tr>
                <td class="success">${name}</td>
              </tr>
            `);
          }
        }
      });
    });
  };
}

const app = new App();
app.start();
