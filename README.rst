Installation
============

Run::

    pip install --user -r requirements.txt

to install the dependencies.

Banner Scraper
==============

- When running the banner scraper (located in ``banner-scraper/scraper.py``, do
  not log in to Trailhead). Also, if you are currently logged in to Trailhead in
  a browser, you will be logged out.

To run the banner scraper::

    cd banner-scraper
    ./scraper

and enter your username, password, and desired output file name.

Manager Selector
================

To run the manager selector, and output it to a file::

    cd manager-selector
    ./manager-selector.py <scraper_output> > <output_filename>

where ``<scraper_output>`` is the output from the banner scraper.
