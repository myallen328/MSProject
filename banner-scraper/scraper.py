#! /usr/bin/env python3
"""
Scrapes GPA and grade data from Banner
"""

import re
import os
import csv
import getpass
from http.cookiejar import CookieJar

import mechanicalsoup
import progressbar

username = input('Username: ')
password = getpass.getpass('Password: ')
output_filename = os.path.expanduser(input('Output Filename: '))

login_url = 'https://trailhead.mines.edu:8447/cas-trailhead-web/login?service=https%3A%2F%2Fssomgr.mines.edu%3A443%2Fssomanager%2Fc%2FSSB'

cookiejar = CookieJar()
browser = mechanicalsoup.StatefulBrowser()
browser.set_cookiejar(cookiejar)
browser.open(login_url)

# Login Form
form = browser.select_form(nr=0)
form['username'] = username
form['password'] = password
browser.submit_selected()

# Term Form
terms = []
browser.open('https://banner.mines.edu/prod/owa/bwlkostm.P_FacSelTerm')
form = browser.select_form(nr=1)
options = browser.get_current_page().find_all('form')[1].find(
    'select').find_all('option')
for option in options:
    terms.append((option.string, option.attrs['value']))

print('Select term:')
for i, term in enumerate(terms):
    print('{}: {}'.format(i, term[0]))

term_index = int(input('Enter selection: '))
form.set_select({'term': terms[term_index][1]})

browser.submit_selected()

# Get the CWIDs from the Class Summary page
browser.open('https://banner.mines.edu/prod/owa/bwlkfcwl.P_FacClaListSum')
form = browser.select_form(nr=1)

# Determine classes
classes = []
options = browser.get_current_page().find_all('form')[1].find(
    'select').find_all('option')
for option in options:
    classes.append((option.string, option.attrs['value']))

print('Select class:')
for i, class_ in enumerate(classes):
    print('{}: {}'.format(i, class_[0]))

class_index = int(input('Enter selection: '))
form.set_select({'crn': classes[class_index][1]})

browser.submit_selected()

students = []

page = browser.get_current_page()
username_re = re.compile('mailto:(.*)@.*')
for tr in page.find_all('tr'):
    # Table rows with student information contain two links.
    if len(tr.find_all('a')) < 2:
        continue

    # Iterate through all of the TDs
    tds = [
        td for td in tr.find_all('td')
        if 'dddefault' in td.attrs.get('class', [])
    ]

    # This is the first few TDs
    if not tds:
        continue

    (_, _, name_td, cwid_td, _, level_td, _, class_td, major_td, *_,
     email_td) = tds
    username = username_re.match(email_td.find('a').attrs.get('href')).group(1)
    students.append({
        'name': name_td.find('a').string,
        'cwid': cwid_td.find('span').string,
        'level': level_td.find('span').string,
        'class': class_td.find('span').string,
        'major': major_td.find('span').string,
        'username': username,
    })

for student in progressbar.progressbar(students):
    student['grades'] = []

    # Student ID Form
    browser.open('https://banner.mines.edu/prod/owa/bwlkoids.P_FacIDSel')
    form = browser.select_form(nr=1)
    form['STUD_ID'] = student['cwid']
    browser.submit_selected()

    # User Confirmation
    form = browser.select_form(nr=1)
    browser.submit_selected()

    # Transcript Type Selection
    browser.open('https://banner.mines.edu/prod/owa/bwlkftrn.P_FacDispTran')
    form = browser.select_form(nr=1)
    browser.submit_selected()

    page = browser.get_current_page()
    first_non_transfer_credit = True
    for tr in page.find_all('tr'):
        tds = tr.find_all('td')

        # Transfer credit (we don't care, just skip)
        if len(tds) == 7:
            continue

        if len(tds) == 6:
            if tr.find('th').string == 'Overall:':
                student['gpa'] = float(tds[-1].text)

        # Non-transfer credit
        if len(tds) == 9:
            # Ignore the first row because it is the page header.
            if first_non_transfer_credit:
                first_non_transfer_credit = False
                continue

            (subject_field, course_field, _, title_field, grade_field, *_) = \
                tr.find_all('td')
            student['grades'].append({
                'subject': subject_field.string,
                'course': course_field.string,
                'title': title_field.string,
                'grade': grade_field.string,
            })

# students array is
# [{
#     name: string,
#     username: string,
#     cwid: string,
#     level: string,
#     class: string,
#     major: string,
#     gpa: float,
#     grades: [{
#         subject: string,
#         course: string,
#         title: string,
#         grade: string
#     }]
# }]

with open(output_filename, 'w+') as f:
    fieldnames = [
        'name', 'username', 'cwid', 'level', 'class', 'major', 'gpa', 'grades'
    ]
    writer = csv.DictWriter(f, fieldnames=fieldnames)

    writer.writeheader()
    for entry in students:
        writer.writerow(dict(entry))
