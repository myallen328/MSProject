#!/usr/bin/env python3
import csv
import sys

from numpy.random import choice

# Check arguments
if len(sys.argv) not in (2, 3):
    print('Usage: ./manager-selector.py <input_filename>')
    sys.exit(1)


class Student(object):
    name = ""
    gradeTotal = 0

    # major = ""

    # The class "constructor" - It's actually an initializer
    def __init__(self, name, gradeTotal):  #, major):
        self.name = name
        self.gradeTotal = gradeTotal
        # self.major = major

    def __repr__(self):
        return '<Student name={} gradeTotal={}>'.format(
            self.name, self.gradeTotal)


def calcAverage(students):
    total = 0
    for x in range(len(students)):
        total += students[x].gradeTotal

    return total / len(students)


def calcStdDev(students, average):
    sumOfSquares = 0
    for x in range(len(students)):
        sumOfSquares += (students[x].gradeTotal - average)**2

    return (sumOfSquares / len(students))**(1 / 2)


allStudents = []

# # Create dummy data
# # TODO use data retrieved from scraper and create an object to house file input
# for x in range(0, 30):
#     newStudent = Student(chr(ord('a') + x), x)
#     allStudents.append(newStudent)

with open(sys.argv[1]) as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=',')
    lineCount = 0

    switcher = {
        'A': 4,
        'A-': 3.7,
        'B+': 3.3,
        'B': 3,
        'B-': 2.7,
        'C+': 2.3,
        'C': 2,
        'C-': 1.7,
        'D+': 1.3,
        'D': 1
    }

    for row in csv_reader:
        name = row['name']
        classTitle = row['class']
        # major = row['major']

        gradeTotal = 0

        for grade in eval(row['grades']):
            gradeTotal += switcher.get((grade.get('grade')), 0)

        if classTitle == 'GRADUATE':
            gradeTotal += 10
        elif classTitle == 'SENIOR':
            gradeTotal += 5
        elif classTitle == 'JUNIOR':
            gradeTotal += 3

        newStudent = Student(name, gradeTotal)
        allStudents.append(newStudent)

# Calculate needed variables for later
average = calcAverage(allStudents)
stdDev = calcStdDev(allStudents, average)

# sort the students so that the 'top' student is last
allStudents.sort(key=lambda x: x.gradeTotal, reverse=False)

# Get the lowest students standard deviation so we can shift everyone right
smallestDev = (allStudents[0].gradeTotal - average) / stdDev - 0.0001

# List for keeping track of deviations.
probabilities = []
totalDevCount = 0

# First, we store the shifted deviations in the probabilities array (not probabilities yet, but ratios are correct)
for x in range(len(allStudents)):

    # subtract the smallest deviation here because it's guaranteed to be a negative number and we want to shift right
    resDev = (allStudents[x].gradeTotal - average) / stdDev - smallestDev
    assert resDev > 0

    totalDevCount += resDev
    probabilities.append(resDev)

# Divide each number in the probabilities array by the total deviation count for the true probability
probabilities = list(map(lambda x: x / totalDevCount, probabilities))

# Get the list of managers based on their probabilities
managerList = choice(
    a=allStudents, size=len(allStudents), replace=False, p=probabilities)

# Quick check to see what the data looks like!
for x in range(len(managerList)):
    print(managerList[x].name + '   ' + str(managerList[x].gradeTotal))
