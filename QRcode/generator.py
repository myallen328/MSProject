# pip install qrcode[pil]
import qrcode
import io

qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_L,
    box_size=10,
    border=4,
)
# test CWID
qr.add_data('12345678')
qr.make(fit=True)

# If we want to customize the color in the end, we can...
img = qr.make_image(fill_color="black", back_color="white")

# # show qr code to screen
# img.show()

# # save picture to local file for testing (sent the file to my phone..)
# byteImg = io.BytesIO()
# img.save(byteImg, format='PNG')
# byteImg = byteImg.getvalue()
#
# with open('qrcode.png', 'wb') as f:
#     f.write(byteImg)
