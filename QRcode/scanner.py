# pip install pyzbar
from pyzbar import pyzbar
import time
# pip install opencv-contrib-python
import cv2

print("[INFO] starting.. press q to quit")

# Begin capturing, then wait two seconds to give it time to boot
vs = cv2.VideoCapture(0)
time.sleep(2.0)

foundCodes = []

while True:
    # Read the current frame
    ret, frame = vs.read()

    # See if any codes are present on screen
    codes = pyzbar.decode(frame)

    for code in codes:
        # Grab the frame of the qrcode and show a border around it
        (x, y, w, h) = code.rect
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)

        # Grab the data from the code
        codeData = code.data.decode("utf-8")

        # Used for testing... if-statement probably not necessary..?
        if codeData not in foundCodes:
            print('FOUND CODE - ' + codeData)
            foundCodes.append(codeData)

    # Show the current frame, detect if q is pressed to quit
    cv2.imshow('frame', frame)
    key = cv2.waitKey(1) & 0xff

    if key == ord("q"):
        break

# clean up
print("cleaning up")
vs.release()
cv2.destroyAllWindows()
