Project Description
###################

:Author: Sumner Evans

I thing the best way to solve second and third challenges described in the
Project Description is to create an automated system for group selection. I
think that creating some web application (accessible from a smartphone) which
students use will help eliminate toil from the process. This application should
be have a way of creating an output which is ingestible by the group selection
algorithm.

Here are a few ideas that I have had regarding this app:

- To mitigate problems arising from absences of hiring managers, students must
  "sign-in" at the beginning of class. Then, 1/3 of them are chosen as hiring
  managers according to the algorithm which ends up being used for challenge 1.

- Non-hiring managers will be shown a QR code on their phone. Hiring managers
  will go around and scan the QR codes of the people they want on their list.
  Once a QR code has been scanned twice, it will disappear.

  This will prevent people from accepting too many offers from hiring managers.
  Additionally, the system will prevent hiring managers from getting too many
  people on their list.

- It seems easiest to back this application by a simple relational database
  which can have endpoints for retrieving information about the groups. These
  endpoints could include endpoints designed for use in visualizations.

- It may be interesting to have a real-time visualization of the group selection
  process.
