Application UX
==============

:Author: Sumner Evans
:Date: 2018-09-05

Phone App Login Page
--------------------

https://wireframe.cc/C7sffz

.. image:: graphics/phone-app-login.png
   :height: 300

Hiring Manager View
-------------------

https://wireframe.cc/wgiwU5

.. image:: graphics/phone-app-hiring-manager.png
   :height: 300

Non-Hiring Manager View
-----------------------

https://wireframe.cc/Wu0TzK

.. image:: graphics/phone-app-non-hiring-manager.png
   :height: 300

Admin Panel
-----------

https://wireframe.cc/wBxVqU

.. image:: graphics/admin-panel.png
   :width: 100%

Admin Finalization
------------------

https://wireframe.cc/pKktad

.. image:: graphics/stages/finalize.png
   :width: 100%
