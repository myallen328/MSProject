%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Jacobs Landscape Poster
% LaTeX Template
% Version 1.1 (14/06/14)
%
% Created by:
% Computational Physics and Biophysics Group, Jacobs University
% https://teamwork.jacobs-university.de:8443/confluence/display/CoPandBiG/LaTeX+Poster
% 
% Further modified by:
% Nathaniel Johnston (nathaniel@njohnston.ca)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This template has been modified by:
%       Jonathan Sumner Evans (jonathanevans@mines.edu)
%       Sam Sartor (ssartor@mines.edu)
%       Robbie Merillat (rdmerillat@mines.edu)


%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[final]{beamer}

\usepackage[scale=1.2]{beamerposter} % Use the beamerposter package for laying out the poster
\usepackage{biblatex}
\usepackage{csquotes}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{booktabs} % Top and bottom rules for tables
\usepackage[export]{adjustbox}
\usepackage{tikz-cd}

\usetheme{confposter} % Use the confposter theme supplied with this template

\addbibresource{../report/references.bib}

\setbeamercolor{block title}{fg=ngreen,bg=white} % Colors of the block titles
\setbeamercolor{block body}{fg=black,bg=white} % Colors of the body of blocks
\setbeamercolor{block alerted title}{fg=white,bg=dblue!70} % Colors of the highlighted block titles
\setbeamercolor{block alerted body}{fg=black,bg=dblue!10} % Colors of the body of highlighted blocks
% Many more colors are available for use in beamerthemeconfposter.sty

%-----------------------------------------------------------
% Define the column widths and overall poster size
% To set effective sepwid, onecolwid and twocolwid values, first choose how many columns you want and how much separation you want between columns
% In this template, the separation width chosen is 0.024 of the paper width and a 4-column layout
% onecolwid should therefore be (1-(# of columns+1)*sepwid)/# of columns e.g. (1-(4+1)*0.024)/4 = 0.22
% Set twocolwid to be (2*onecolwid)+sepwid = 0.464

\newlength{\sepwid}
\newlength{\onecolwid}
\newlength{\twocolwid}
\setlength{\paperwidth}{48in} % A0 width: 46.8in
\setlength{\paperheight}{36in} % A0 height: 33.1in
\setlength{\sepwid}{0.02\paperwidth} % Separation width (white space) between columns
\setlength{\onecolwid}{0.23\paperwidth} % Width of one column
\setlength{\twocolwid}{0.46\paperwidth} % Width of two columns
\setlength{\topmargin}{-0.5in} % Reduce the top margin size

%-----------------------------------------------------------

\title{Automating Group Formation for Optimally Competitive Groups}
\author{Jonathan Sumner Evans and Mykel Allen}
\institute{Department of Computer Science, Colorado School of Mines}

\begin{document}

    \addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks
    \addtobeamertemplate{block alerted end}{}{\vspace*{2ex}} % White space under highlighted (alert) blocks

    \setlength{\belowcaptionskip}{2ex} % White space under figures
    \setlength\belowdisplayshortskip{2ex} % White space under equations

    \begin{frame}[t] % The whole poster is enclosed in one beamer frame
        % The whole poster consists of three major columns, the second of which
        % is split into two columns twice - the [t] option aligns each column's
        % content to the top
        \begin{columns}[t]
            \begin{column}{\sepwid}\end{column} % Empty spacer column

            \begin{column}{\onecolwid} % The first column

                \begin{block}{Background and Motivation}
                    \setlength{\parskip}{0.5em}

                    In CSCI 406 Algorithms, Dr. Mehta uses a process for
                    creating project groups in which $1/3$ of the students are
                    chosen as \textbf{hiring managers}. The hiring managers
                    \textit{hire} four people; and each \textbf{candidate}
                    accepts hire requests from two managers.

                    Dr. Mehta then creates groups by matching each hiring
                    manager with two of the people who they hired. He uses an
                    algorithm similar to network flow to do this.

                    \begin{figure}
                        \centering
                        \begin{subfigure}{.5\textwidth}
                            \centering
                            \includegraphics[width=\linewidth]{../graphics/class2-eps-converted-to}
                            \caption{Hiring Managers and Candidate Pool}
                            \label{fig:managers-candidates}
                        \end{subfigure}%
                        \begin{subfigure}{.5\textwidth}
                            \centering
                            \includegraphics[width=0.45\linewidth]{../graphics/matching2-eps-converted-to}
                            \caption{Matched Hiring Managers and Candidates}
                            \label{fig:matched}
                        \end{subfigure}
                        \caption{Group Selection Process}
                        \label{fig:process}
                    \end{figure}
                    \vspace{-1.5em}
                    Currently, the hiring process is done by-hand on paper and
                    has many potential failure points including students
                    misunderstanding the process, and instructors mis-entering
                    the data for use in the algorithm.

                \end{block}

                \begin{block}{Goals}
                    \setlength{\parskip}{0.5em}

                    Our project is a comprehensive software solution which will
                    \begin{itemize}
                        \item \textbf{Automate manager selection} from the pool
                            of students.
                        \item \textbf{Create apps for students to use to hire
                                and be hired} to prevent mistakes by students.
                        \item \textbf{Automatically store hiring data} to
                            prevent mistakes by instructors.
                        \item \textbf{Improve the algorithm} which is used to
                            match hiring managers and candidates.
                    \end{itemize}

                    % TODO make this section better

                \end{block}

            \end{column} % End of the first column

            \begin{column}{\sepwid}\end{column} % Empty spacer column

            \begin{column}{\twocolwid} % Start of column 2

                \vspace{-1.3em}
                \begin{columns}[t]
                    \begin{column}{0.95\onecolwid}

                        \begin{block}{Banner Scraper}
                            \setlength{\parskip}{0.5em}

                            In order to get information about students to be used in our manager selector, we created a program which scrapes data
                            from the Banner (Trailhead) website.

                            Written in Python using MechanicalSoup, this system can be seen as a major improvement over the current system which
                            requires the instructor to manually retrieve data
                            about each student from Banner.

                        \end{block}

                    \end{column}
                    \begin{column}{0.05\onecolwid}\end{column} % Empty spacer column

                    \begin{column}{0.95\onecolwid}

                        \begin{block}{Manager Selection}
                            \setlength{\parskip}{0.5em}

                            Once information on the students is gathered, it is then processed by our manager selector in order to determine a "score" for each student. Different factors can affect how each student is rated, such as the amount of CS classes taken or their class standing.
                            
                            After we have a score for each student, the mean and standard deviation of the sample are taken, and used to create a normal distribution in which students are randomly selected given probabilities assigned due to their score.
                        \end{block}

                    \end{column}

                \end{columns}

                \begin{block}{Day of Process}
                    \setlength{\parskip}{0.5em}

                    During the day of in-class group selection for each section
                    of Algorithms, there are three main stages.

                    \begin{columns}[t]
                        \begin{column}{0.3\twocolwid}
                            \begin{block}{Active}
                                \includegraphics[width=\textwidth,frame]{../graphics/stages/active}

                                The instructor can see who has signed in.
                                \vspace{4em}

                                \begin{center}
                                    \includegraphics[width=0.4\textwidth]{../graphics/phone-app-login}
                                \end{center}

                                Students can sign in to the system with their
                                CWID.

                            \end{block}
                        \end{column}

                        \begin{column}{0.3\twocolwid}
                            \begin{block}{Selection Process}
                                \includegraphics[width=\textwidth,frame]{../graphics/stages/selection-process}

                                The instructor can see how many people each
                                manager has hired and how many times each
                                candidate has been hired.
                                \vspace{2em}

                                \begin{center}
                                    \includegraphics[width=0.4\textwidth]{../graphics/phone-app-hiring-manager}%
                                    \hspace{1em}
                                    \includegraphics[width=0.4\textwidth]{../graphics/phone-app-non-hiring-manager-barcode}
                                \end{center}

                                Students are told whether they are a manager or
                                a candidate, and  the app shows them the correct
                                user interface.

                            \end{block}
                        \end{column}

                        \begin{column}{0.3\twocolwid}
                            \begin{block}{Finalized}
                                \includegraphics[width=\textwidth,frame]{../graphics/stages/finalize}

                                The instructor can now see the results of the
                                group selection process and manually put people
                                who were absent into groups.
                                \vspace{2em}

                                \begin{center}
                                    \includegraphics[width=0.4\textwidth]{../graphics/app-thank-you}
                                \end{center}

                                Students are thanked for their participation in the
                                process.

                            \end{block}
                        \end{column}
                    \end{columns}

                \end{block}

            \end{column} % End of column 2

            \begin{column}{\sepwid}\end{column} % Empty spacer column

            \begin{column}{\onecolwid} % Start of third column

                \begin{block}{Current Work}
                    \setlength{\parskip}{0.5em}

                    Our current focus is ensuring that the group selection
                    machinery is ready to be used during the group selection
                    process this semester.

                    We are preparing to test the process in two sections of Dr.
                    Mehta's Algorithms class this semester. There are over 100
                    students between the two sections of Algorithms, and this
                    will be an effective test of our system.

                    In order to determine how well our application works, we
                    intend to collect data about students' experience using the
                    system via a survey.

                \end{block}

                \begin{block}{Future Work}
                    \setlength{\parskip}{0.5em}

                    After testing our system and collecting data about the
                    students' experience using the software, we intend to
                    iterate and improve the system with their feedback.

                    We will also implement a convenient user interface for
                    manually handling students who are absent during the group
                    selection day. This interface will also provide a UI for
                    dealing with situations where the number of students in the
                    class is not divisible by 3.

                    We plan to use the data from the Banner scraper to optimize
                    the groups so that they are as even as possible.

                    Additionally, we would like to add configuration options to
                    the system so that it can be used in other contexts such as
                    different classes or types of projects.

                \end{block}

                \setbeamercolor{block alerted title}{fg=black,bg=norange} % Change the alert block title colors
                \setbeamercolor{block alerted body}{fg=black,bg=white} % Change the alert block body colors

                \vspace{1em}
                \begin{alertblock}{More Information}

                    \textbf{Project:} \\
                    \href{https://gitlab.com/myallen328/MSProject}{\url{gitlab.com/myallen328/MSProject}}

                \end{alertblock}

                \vspace{1em}

                \includegraphics[width=\textwidth]{../graphics/csam}

                \vspace{1em}

                \includegraphics[width=\textwidth]{../graphics/dept-of-cs}
            \end{column} % End of the third column

        \end{columns} % End of all the columns in the poster

    \end{frame} % End of the enclosing frame

\end{document}
