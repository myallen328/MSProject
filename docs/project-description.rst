Project Description
###################

:Author: Sumner Evans

Problem Overview
================

The group selection process for CSCI 406 Algorithms is a tedious, error-prone
process. Additionally, the algorithm which is used to create teams is
inefficient. The current process is as follows:

1. The instructor selects one third of the students to be "hiring managers"
   (this process is somewhat tedious).
2. On the "group selection" day, the hiring managers go around and ask 4 people
   to be on their list. People who are not hiring managers can only accept an
   offer from two people. At the end of class, the hiring managers give the
   instructor their list.
3. The instructor manually enters the data from the hiring managers.
4. The instructor runs an algorithm to determine the groups.
5. The instructor manually fixes issues such as a number of students that is not
   a multiple of 3.

This process has a lot of room for human error. A few examples include:

- Absences of hiring managers
- Non-hiring managers accepting offers from more than 2 hiring managers.
- Hiring managers getting more than 4 people on their lists.
- Transcription errors by the instructor from the hiring managers' written
  lists.

Additionally, the algorithm which is used for group creating is slow, and does
not easily handle numbers of students which are not multiples of 3.

There appears to be six main challenges which are interesting to solve:

1. The selection of hiring managers.
2. Mitigating human error in the group selection process.
3. Eliminating errors with converting physical records to electronic ones
   suitable for pushing through the algorithm.
4. Hardening the algorithm for group creation so that it is resilient to numbers
   of students which are not multiples of three.
5. Making the algorithm more efficient.
6. Visualizations of various aspects of this process.

Plan of Action
==============

Our plan is to tackle the first three challenges for the Fall semester. That
way, we can try the new group selection process in the Spring semester. We plan
to solve the fourth and fifth challenges in the Spring semester.
